package cn.cerc.mis.core;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface IPage extends IOriginOwner {

    IForm getForm();

    String execute() throws ServletException, IOException;

    default HttpServletRequest getRequest() {
        IForm form = getForm();
        return form != null ? form.getRequest() : null;
    }

    default HttpServletResponse getResponse() {
        IForm form = getForm();
        return form != null ? form.getResponse() : null;
    }

    default Object setMessage(String message) {
        return null;
    }
}
