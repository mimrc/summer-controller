package cn.cerc.mis.core;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.Utils;
import cn.cerc.db.log.KnowallException;
import cn.cerc.db.tool.LogUtils;
import cn.cerc.mis.client.ServiceExecuteException;
import cn.cerc.mis.other.PageNotFoundException;
import cn.cerc.mis.security.SecurityStopException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface IErrorPage {
    Logger log = LoggerFactory.getLogger(IErrorPage.class);

    default void output(Throwable throwable) {
        var request = getRequest();
        String clientIP = AppClient.getClientIP(request);
        String message = throwable.getMessage();

        if (throwable.getCause() != null) {
            throwable = throwable.getCause();
            message = throwable.getMessage();
        }

        String method = request.getMethod();
        String url = this.getUrl(request);
        String clazz = throwable.getClass().getName();

        if (throwable instanceof PageNotFoundException)
            log.info("找不到页面 {}", message);
        else if (throwable instanceof UserRequestException)
            log.info("请求异常 {}", message, new RequestFailException(throwable, clientIP, method, url));
        else if (throwable instanceof NoSuchMethodException)
            log.info("找不到方法 {}", message, new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof IllegalArgumentException)
            log.info("参数数量异常 {}", message, new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof UnsupportedOperationException)
            log.info("异常操作 {}", message, new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof DataException)
            log.warn("数据校验失败 {}", message, new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof SecurityStopException)
            log.warn("用户权限不足 {}", message);
        else if (throwable instanceof IOException) {
            if ("ClientAbortException".equals(throwable.getClass().getSimpleName())) {
                log.warn("客户端断开连接 {}", message, new KnowallException(throwable).add(clientIP).add(method).add(url));
                return;
            } else {
                log.error("IO异常 {}", message, new KnowallException(throwable).add(clientIP).add(method).add(url));
            }
        } else if (throwable instanceof ServiceExecuteException)
            log.error(Lang.get(IErrorPage.class, 10, "服务执行异常 {}"), message,
                    new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof ServletException)
            log.error(Lang.get(IErrorPage.class, 11, "servlet异常 {}"), message,
                    new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof ReflectiveOperationException)
            log.error(Lang.get(IErrorPage.class, 12, "反射异常 {}"), message,
                    new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof NullPointerException)
            log.error(Lang.get(IErrorPage.class, 13, "空指针异常 {}"), message,
                    new KnowallException(throwable).add(clientIP).add(method).add(url));
        else if (throwable instanceof KnowallException knowall)
            log.error(throwable.getMessage(), knowall.add(clientIP).add(method).add(url).add(clazz));
        else if (throwable instanceof RuntimeException) {
            log.error(throwable.getMessage(),
                    new KnowallException(throwable).add(clientIP).add(method).add(url).add(clazz));
        } else {
            log.error(Lang.get(IErrorPage.class, 15, "未知的异常 {} -> {}"), clazz, message,
                    new KnowallException(throwable).add(clientIP).add(method).add(url).add(clazz));
        }

        String errorPage = this.getErrorPage(throwable);
        if (errorPage != null) {
            String path = String.format("/WEB-INF/forms/%s", errorPage);
            try {
                var response = getResponse();
                request.getServletContext().getRequestDispatcher(path).forward(request, response);
            } catch (ServletException | IOException e) {
                LogUtils.error(e.getMessage());
            }
        }
    }

    private String getUrl(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        String site = request.getRequestURL().toString();
        builder.append(site);
        Map<String, String> items = this.convert(request.getParameterMap());
        int i = 0;
        for (String key : items.keySet()) {
            i++;
            builder.append(i == 1 ? "?" : "&");
            builder.append(key);
            builder.append("=");
            String value = items.get(key);
            if (value != null) {
                builder.append(Utils.encode(value, StandardCharsets.UTF_8.name()));
            }
        }
        return builder.toString();
    }

    /**
     * 解析 request 的请求参数
     */
    private Map<String, String> convert(Map<String, String[]> params) {
        Map<String, String> items = new LinkedHashMap<>();
        params.forEach((key, value) -> {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < value.length; i++) {
                builder.append(value[i]);
                if (i < value.length - 1) {
                    builder.append(",");
                }
            }
            items.put(key, builder.toString());
        });
        return items;
    }

    String getErrorPage(Throwable error);

    HttpServletRequest getRequest();

    HttpServletResponse getResponse();
}
