package cn.cerc.mis.core;

import cn.cerc.db.core.IHandle;

public interface ServiceSupplier {

    IService findService(IHandle handle, String serviceId);

}
