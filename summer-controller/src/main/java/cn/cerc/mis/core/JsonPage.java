package cn.cerc.mis.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.MediaType;

import cn.cerc.db.core.ISession;
import cn.cerc.db.tool.JsonTool;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletResponse;

public class JsonPage implements IPage {
    protected IForm origin;
    private Object data;
    private Map<String, Object> items = null;

    public JsonPage() {
        super();
    }

    public JsonPage(IForm form) {
        super();
        this.setOrigin(form);
    }

    @Deprecated
    public JsonPage(IForm form, Object data) {
        super();
        this.setOrigin(form);
        this.data = data;
    }

    @Override
    public IForm getForm() {
        return origin;
    }

    @Override
    public JsonPage setOrigin(Object form) {
        this.origin = (IForm) form;
        return this;
    }

    @Override
    public IForm getOrigin() {
        return this.origin;
    }

    @Override
    public String execute() throws ServletException, IOException {
        IForm form = this.getForm();
        var obj = form.getSession().getProperty(ISession.RESPONSE);
        if (obj instanceof HttpServletResponse response) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            PrintWriter writer = response.getWriter();
            writer.print(this.json());
        } else {
            throw new RuntimeException("not support class: " + obj.getClass().getName());
        }
        return null;
    }

    public String json() {
        String json;
        if (this.data == null) {
            if (items == null) {
                items = new LinkedHashMap<>();
            }
            json = JsonTool.toJson(items);
        } else {
            json = JsonTool.toJson(this.data);
        }
        return json;
    }

    @Deprecated
    public JsonPage add(String key, Object value) {
        return put(key, value);
    }

    public JsonPage put(String key, Object value) {
        if (this.data != null) {
            throw new RuntimeException("data is not null");
        }
        if (items == null) {
            items = new LinkedHashMap<>();
        }
        items.put(key, value);
        return this;
    }

    public Object getData() {
        return data;
    }

    public JsonPage setData(Object data) {
        if (this.items != null) {
            throw new RuntimeException("data is not null, json page items must be null.");
        }
        this.data = data;
        return this;
    }

    public JsonPage setResultMessage(boolean result, String message) {
        this.put("result", result);
        this.put("message", message);
        return this;
    }

    public Map<String, Object> getItems() {
        if (items == null) {
            items = new LinkedHashMap<>();
        }
        return items;
    }

    public void setItems(Map<String, Object> items) {
        this.items = items;
    }

}
