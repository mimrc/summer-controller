package cn.cerc.mis.core;

import cn.cerc.db.core.ISession;

public interface FormSupplier {

    IForm getForm(ISession session, String formId, String funcCode);

}
