package cn.cerc.mis.core;

import java.lang.reflect.InvocationTargetException;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Variant;
import cn.cerc.mis.tools.ServiceUtils;

public interface IService {

    default DataSet _call(IHandle handle, DataSet dataIn, Variant function)
            throws IllegalAccessException, InvocationTargetException, DataException, RuntimeException {
        return ServiceUtils.call(this, handle, dataIn, function);
    }

    // 仅用于 Delphi Client 调用
    @Deprecated
    default String getJSON(DataSet dataOut) {
        return String.format("[%s]", dataOut.json());
    }

}
