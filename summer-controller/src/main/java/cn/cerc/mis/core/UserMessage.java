package cn.cerc.mis.core;

import cn.cerc.db.core.IHandle;
import cn.cerc.mis.message.MessageLevel;
import cn.cerc.mis.message.MessageProcess;

/**
 * 数据库消息队列
 * 
 * @author ZhangGong
 *
 */
public interface UserMessage {

    /**
     * 给用户发送消息
     * 
     * @param handle
     * @param toUserCode
     * @param subject
     * @param content
     * @param messageType 消息类别，一般是发送的class代码
     * @return
     */
    default String send(IHandle handle, String toUserCode, String subject, String content, String messageType) {
        return send(handle, handle.getCorpNo(), toUserCode, MessageLevel.General, subject, content, MessageProcess.stop,
                messageType);
    }

    /**
     * 增加新的消息，并返回消息编号（msgID）
     * 
     * @param corpNo   帐套代码
     * @param userCode 用户代码
     * @param level    消息等级
     * @param subject  消息标题
     * @param content  消息内容
     * @param process  处理进度
     * @param UIClass  消息类别
     * @return 返回消息编号（msgID）
     */
    String send(IHandle handle, String corpNo, String userCode, MessageLevel level, String subject, String content,
            MessageProcess process, String UIClass);

}
