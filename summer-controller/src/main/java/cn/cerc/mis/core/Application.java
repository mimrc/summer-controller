package cn.cerc.mis.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.Variant;

/**
 * 请改使用 SpringBean
 * 
 * 注意：不得随意在SpringBean上增加方法!
 */
@Component
public class Application implements ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    // 签核代理用户列表，代理多个用户以半角逗号隔开
    public static final String ProxyUsers = "ProxyUsers";
    // 客户端代码
    public static final String ClientIP = "clientIP";
    // 本地会话登录时间
    public static final String LoginTime = "loginTime";
    // 浏览器通用客户设备Id
    public static final String WebClient = "webclient";
    // spring context
    private static ApplicationContext context;

    public static ApplicationContext init() {
        initFromXml("application.xml");
        return context;
    }

    public static ApplicationContext initFromXml(String springXmlFile) {
        if (context == null)
            setContext(new ClassPathXmlApplicationContext(springXmlFile));
        return context;
    }

    /**
     * 根据 SummerConfiguration.class 初始化 spring context
     * 
     * @return ApplicationContext context
     */
    public static ApplicationContext initOnlyFramework() {
        return init(SummerSpringConfiguration.class);
    }

    public static ApplicationContext init(Class<?>... annotatedClasses) {
        if (context == null) {
            // FIXME: 自定义作用域，临时解决 request, session 问题
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(annotatedClasses);
            RequestScope scope = new RequestScope();
            context.getBeanFactory().registerScope(RequestScope.REQUEST_SCOPE, scope);
            context.getBeanFactory().registerScope(RequestScope.SESSION_SCOPE, scope);

            setContext(context);
        }
        return context;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Application.setContext(applicationContext);
    }

    public static void setContext(ApplicationContext applicationContext) {
        if (context != applicationContext) {
            if (context != null) {
                log.error("applicationContext overload!");
            }
            context = applicationContext;
        }
    }

    @Deprecated
    public static ApplicationContext getContext() {
        return context;
    }

    public static boolean containsBean(Class<?> clazz) {
        if (context == null)
            return false;
        return context.getBeanNamesForType(clazz).length > 0;
    }

    public static boolean containsBean(String name) {
        if (context == null)
            return false;
        return context.containsBean(name);
    }

    public static <T> T getBean(String beanId, Class<T> requiredType) {
        if (!context.containsBean(beanId))
            return null;
        return context.getBean(beanId, requiredType);
    }

    public static <T> T getBean(Class<T> requiredType) {
        if (context == null) {
            var e = new RuntimeException("context is null, getBean return null: " + requiredType.getSimpleName());
            log.error(e.getMessage(), e);
            return null;
        }
        try {
            return context.getBean(requiredType);
        } catch (BeansException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T getBean(IHandle handle, Class<T> requiredType) {
        T bean = getBean(requiredType);
        if (bean instanceof IHandle temp)
            temp.setSession(handle.getSession());
        return bean;
    }

    public static Object getBean(IHandle handle, String beanId) {
        Object bean = getBean(beanId, Object.class);
        if (bean instanceof IHandle temp)
            temp.setSession(handle.getSession());
        return bean;
    }

    /**
     * 返回指定的service对象，若为空时会抛出 ClassNotFoundException
     * 
     * @param handle      IHandle
     * @param serviceCode 服务代码
     * @param function    KeyValue
     * @return Service bean
     * @throws ClassNotFoundException 类文件异常
     */
    @Deprecated
    public static Object getService(IHandle handle, String serviceCode, Variant function)
            throws ClassNotFoundException {
        return SpringBean.get(ServiceFactory.class).get(handle, serviceCode, function);
    }

    public static String getBeanIdOfClassCode(String classCode) {
        if (classCode.length() < 2)
            return classCode.toLowerCase();
        var temp = classCode;
        var first = classCode.substring(0, 2);
        if (!first.toUpperCase().equals(first))
            temp = classCode.substring(0, 1).toLowerCase() + classCode.substring(1);
        return temp;
    }

}
