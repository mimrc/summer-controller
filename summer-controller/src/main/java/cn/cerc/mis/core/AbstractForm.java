package cn.cerc.mis.core;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import cn.cerc.db.core.ISession;
import cn.cerc.db.core.SpringBean;
import cn.cerc.mis.client.ServiceExecuteException;
import cn.cerc.mis.plugins.PluginFactory;
import cn.cerc.mis.security.Permission;
import cn.cerc.mis.security.Webform;
import cn.cerc.mis.tools.FormUtils;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;

public abstract class AbstractForm implements IForm, InitializingBean {

    private String id;
    private ISession session;

    private Map<String, String> params = new HashMap<>();
    private String name;
    private String permission;
    private String module;
    private String[] pathVariables;
    private String beanName;
    private AppClient client;

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public AppClient getClient() {
        return this.client;
    }

    public Object getProperty(String key) {
        if ("request".equals(key))
            return this.getRequest();
        if ("session".equals(key))
            return this.getSession();
        return this.getSession().getProperty(key);
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Deprecated
    public void setCaption(String name) {
        setName(name);
    }

    @Override
    public void setParam(String key, String value) {
        params.put(key, value);
    }

    @Override
    public String getParam(String key, String def) {
        return params.getOrDefault(key, def);
    }

    @Override
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    // 执行指定函数，并返回jsp文件名，若自行处理输出则直接返回null
    protected String callDefault(String funcCode)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, ServletException, IOException, ServiceExecuteException {
        var page = SpringBean.get(FormUtils.class).get(this, funcCode, this.pathVariables);
        List<PagePlugin> plugins = PluginFactory.getPlugins(page.getForm(), PagePlugin.class);
        for (var plugin : plugins)
            plugin.call(String.join(".", this.getClass().getSimpleName(), funcCode), page);
        return page.execute();
    }

    protected Method findMethod(Class<? extends AbstractForm> clazz, String funcCode) {
        for (Method item : clazz.getMethods()) {
            if (funcCode.equals(item.getName()))
                return item;
        }
        return null;
    }

    @Override
    public void setPathVariables(String[] pathVariables) {
        this.pathVariables = pathVariables;
    }

    public String[] getPathVariables() {
        return this.pathVariables;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getBeanName() {
        return beanName;
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public ISession getSession() {
        return this.session;
    }

    @Override
    public void setSession(ISession session) {
        this.session = session;
    }

    @Override
    public void afterPropertiesSet() {
        Webform obj = this.getClass().getAnnotation(Webform.class);
        if (obj != null) {
            this.name = obj.name();
            this.module = obj.module();
        }
        Permission ps = this.getClass().getAnnotation(Permission.class);
        if (ps != null) {
            this.permission = ps.value();
        }
    }

    @Resource
    public void setAppClient(AppClient client) {
        this.client = client;
    }
}
