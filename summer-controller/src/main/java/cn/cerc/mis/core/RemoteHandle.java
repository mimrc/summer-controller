package cn.cerc.mis.core;

import java.util.HashMap;
import java.util.Map;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.ISession;

public class RemoteHandle implements IHandle {
    private ISession session;
    private Map<String, Object> params = new HashMap<>();
    private String originCorpNo;
    private String originUserCode;

    public RemoteHandle(IHandle handle, String corpNo) {
        init(handle.getSession(), corpNo);
    }

    public RemoteHandle(ISession owner, String corpNo) {
        init(owner, corpNo);
    }

    private void init(ISession owner, String corpNo) {
        this.originCorpNo = owner.getCorpNo();
        this.originUserCode = owner.getUserCode();
        this.session = new ISession() {
            @Override
            public Object getProperty(String key) {
                if (params.containsKey(key))
                    return params.get(key);
                else
                    return owner.getProperty(key);
            }

            @Override
            public boolean logon() {
                return owner.logon();
            }

            @Override
            public void close() {
                ISession.super.close();
                owner.close();
            }

            @Override
            public String getPermissions() {
                return owner.getPermissions();
            }

            @Override
            public Map<String, Object> getProperties() {
                return params;
            }
        };
        this.session.setProperty(ISession.CORP_NO, corpNo);
    }

    public RemoteHandle setUserCode(String userCode) {
        session.setProperty(ISession.USER_CODE, userCode);
        return this;
    }

    public RemoteHandle setUserName(String userName) {
        session.setProperty(ISession.USER_NAME, userName);
        return this;
    }

    @Override
    public ISession getSession() {
        return session;
    }

    @Override
    public void setSession(ISession session) {
        throw new RuntimeException("CorpHandle not support setSession.");
    }

    public String getOriginCorpNo() {
        return originCorpNo;
    }

    public String getOriginUserCode() {
        return originUserCode;
    }

    public boolean isLocal() {
        return this.originCorpNo.equals(this.getCorpNo());
    }
}
