package cn.cerc.mis.core;

import cn.cerc.mis.plugins.Plugin;

public interface PagePlugin extends Plugin {

    void call(String pageCode, IPage page);

}
