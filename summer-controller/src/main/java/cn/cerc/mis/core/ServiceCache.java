package cn.cerc.mis.core;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ServiceCache {

    /**
     * 过期时间，单位为秒，默认1小时，如果设为0则表示关闭
     */
    long expire() default 3600;

    ServiceCacheLevel level() default ServiceCacheLevel.token;

}
