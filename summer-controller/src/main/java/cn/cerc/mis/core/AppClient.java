package cn.cerc.mis.core;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.ConfigReader;
import cn.cerc.db.core.ISession;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.Utils;
import cn.cerc.db.core.Variant;
import cn.cerc.db.redis.Redis;
import cn.cerc.db.redis.RedisRecord;
import cn.cerc.db.tool.JsonTool;
import cn.cerc.mis.other.MemoryBuffer;
import jakarta.annotation.Resource;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AppClient implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(AppClient.class);

    @Serial
    private static final long serialVersionUID = -3593077761901636920L;

    // 缓存版本
    public static final int Version = 1;
    public static final String COOKIE_ROOT_PATH = "/";

    // 手机
    public static final String phone = "phone";
    public static final String android = "android";
    public static final String iphone = "iphone";
    public static final String wechat = "weixin";

    // GPS
    public static final String gps_pkg = ConfigReader.instance().getProperty("app.gps.pkgId", "");

    /**
     * 类手机终端
     */
    public static final List<String> phone_devices = new ArrayList<>();

    static {
        phone_devices.add(AppClient.phone);
        phone_devices.add(AppClient.android);
        phone_devices.add(AppClient.iphone);
        phone_devices.add(AppClient.wechat);
    }

    // 平板
    public static final String pad = "pad";
    // 电脑
    public static final String pc = "pc";
    // 看板
    public static final String kanban = "kanban";
    // 客户端专用浏览器
    public static final String ee = "ee";
    // execl数据源
    public static final String execl = "execl";

    private HttpServletRequest request;

    private String cookieId;

    private String key;

    private String token;

    private String device;

    private String deviceId = "";

    private String pkgId;

    private HttpServletResponse response;

    public void setToken(String value) {
        this.token = value;
    }

    public String getToken() {
        this.initCookie();
        return token;
    }

    private void initCookie() {
        Variant variant = new Variant();
        this.createCookie(variant);
        this.cookieId = variant.getString();

        this.key = MemoryBuffer.buildObjectKey(AppClient.class, this.cookieId, AppClient.Version);

        Cookie[] cookies = request.getCookies();
        try (Redis redis = new Redis()) {
            this.device = request.getParameter(ISession.CLIENT_DEVICE);
            if (!Utils.isEmpty(device))
                redis.hset(key, ISession.CLIENT_DEVICE, device);
            else {
                this.device = redis.hget(key, ISession.CLIENT_DEVICE);
                if (Utils.isEmpty(device))
                    device = pc;
            }

            this.deviceId = request.getParameter(ISession.CLIENT_ID);
            if (!Utils.isEmpty(deviceId))
                redis.hset(key, ISession.CLIENT_ID, deviceId);
            else {
                this.deviceId = redis.hget(key, ISession.CLIENT_ID);

                if (Utils.isEmpty(deviceId)) {
                    if (cookies != null) {
                        for (Cookie cookie : request.getCookies()) {
                            if (cookie.getName().equals(ISession.CLIENT_ID)) {
                                this.deviceId = cookie.getValue();
                                break;
                            }
                        }
                    }
                }
            }

            this.pkgId = request.getParameter(ISession.PKG_ID);
            if (!Utils.isEmpty(pkgId))
                redis.hset(key, ISession.PKG_ID, pkgId);
            else
                this.pkgId = redis.hget(key, ISession.PKG_ID);

            var sid = request.getParameter(ISession.TOKEN);
            if (!Utils.isEmpty(sid)) {
                redis.hset(key, ISession.TOKEN, sid);
            } else
                sid = redis.hget(key, ISession.TOKEN);

            // token 建立则绑定 cookie
            if (Utils.isNotEmpty(sid)) {
                String userKey = MemoryBuffer.buildKey(SystemBuffer.Token.UserInfoHash, sid);
                if (redis.exists(userKey)) {
                    redis.hset(userKey, ISession.COOKIE_ID, cookieId);
                    redis.expire(userKey, RedisRecord.TIMEOUT);
                }
            }
            this.setToken(sid);
            redis.expire(key, RedisRecord.TIMEOUT);// 每次取值延长生命值
        } catch (IllegalStateException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 根据 request 生成 cookieId
     */
    private boolean createCookie(Variant variant) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals(ISession.COOKIE_ID)) {
                    variant.setValue(cookie.getValue());
                    break;
                }
            }
        }

        if (!variant.isModified()) {
            // 默认域是登录访问网址，过期时间是浏览器关闭
            String cookieId = Utils.getGuid();
            Cookie cookie = new Cookie(ISession.COOKIE_ID, cookieId);
            cookie.setPath(COOKIE_ROOT_PATH);
            cookie.setHttpOnly(true);
            if (response != null)
                response.addCookie(cookie);
            variant.setValue(cookieId);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 读取 cookie 中的 id
     */
    public String getCookieId() {
        this.initCookie();
        return this.cookieId;
    }

    public String key() {
        this.initCookie();
        return this.key;
    }

    public void delete(String field) {
        try (Redis redis = new Redis()) {
            redis.hdel(key, field);
        }
    }

    /**
     * 设备代码
     * 
     * @return
     */
    public String getId() {
        this.initCookie();
        if (this.deviceId == null)
            this.deviceId = "";
        return this.deviceId;
    }

    public void setId(String value) {
        this.initCookie();
        this.deviceId = value == null ? "" : value;
        request.setAttribute(ISession.CLIENT_ID, deviceId);
        try (Redis redis = new Redis()) {
            redis.hset(key, ISession.CLIENT_ID, deviceId);
        }
        if (value != null && value.length() == 28)// 微信openid的长度
            setDevice(phone);
    }

    /**
     * 设备类型默认是 pc
     */
    public String getDevice() {
        this.initCookie();
        return Utils.isEmpty(device) ? pc : device;
    }

    public void setDevice(String value) {
        this.initCookie();
        this.device = Utils.isEmpty(value) ? pc : value;
        request.setAttribute(ISession.CLIENT_DEVICE, device);

        try (Redis redis = new Redis()) {
            redis.hset(key, ISession.CLIENT_DEVICE, device);
        }
    }

    public String getPkgId() {
        this.initCookie();
        if (this.pkgId == null)
            this.pkgId = "";
        return this.pkgId;
    }

    @Deprecated
    public String getLanguage() {
        return Lang.id();
    }

    public boolean isPhone() {
        return phone_devices.contains(getDevice());
    }

    public boolean isKanban() {
        return kanban.equals(getDevice());
    }

    /**
     * 判断当前设备是否是pad
     * 
     * @return true
     */
    public boolean isPad() {
        return pad.equals(getDevice());
    }

    /**
     * 检查当前的token设备是否是GPS应用
     */
    public boolean isGPS() {
        if (Utils.isEmpty(this.getPkgId()))
            return false;
        return gps_pkg.contains(this.getPkgId());
    }

    /**
     * 检查当前的token设备是否是GPS应用
     */
    public static boolean isGPS(String pkgId) {
        if (Utils.isEmpty(pkgId))
            return false;
        return gps_pkg.contains(pkgId);
    }

    /**
     * 判断当时是否为Execl数据源环境
     * 
     * @return
     */
    public boolean isExcel() {
        return execl.equals(getDevice());
    }

    /**
     * 获取客户端真实IP地址，不直接使用request.getRemoteAddr() 的原因是有可能用户使用了代理软件方式避免真实IP地址
     * <p>
     * x-forwarded-for 是一串IP值，取第一个非unknown的有效IP字符串为客户端的真实IP
     * <p>
     * 如：x-forwarded-for：192.168.1.110, 192.168.1.120, 192.168.1.130, 192.168.1.100
     * <p>
     * 用户真实IP为： 192.168.1.110
     *
     * @param request HttpServletRequest
     * @return IP地址
     */
    public static String getClientIP(HttpServletRequest request) {
        if (request == null)
            return "";
        try {
            String ip = request.getHeader("x-forwarded-for");
            if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip))
                ip = request.getHeader("Proxy-Client-IP");
            if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip))
                ip = request.getHeader("WL-Proxy-Client-IP");
            if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip))
                ip = request.getHeader("HTTP_CLIENT_IP");
            if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip))
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip))
                ip = request.getRemoteAddr();
            if ("0:0:0:0:0:0:0:1".equals(ip))
                ip = "0.0.0.0";
            // 以第一个IP地址为用户的真实地址
            String[] arr = ip.split(",");
            ip = Arrays.stream(arr).findFirst().orElse("").trim();
            return ip;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "";
        }
    }

    @Override
    public String toString() {
        Map<String, String> items;
        try (Redis redis = new Redis()) {
            items = redis.hgetAll(key);
        }
        return JsonTool.toJson(items);
    }

    @Resource
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Resource
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }
}
