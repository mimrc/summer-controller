package cn.cerc.mis.core;

import java.util.ArrayList;
import java.util.List;

import cn.cerc.db.core.DataException;
import cn.cerc.db.exception.IKnowall;

/**
 * 用户操作 -> 条件限制
 */
public class UserOperateLimitException extends DataException implements IKnowall {

    private static final long serialVersionUID = 1872713023920247010L;

    private int limit;

    public UserOperateLimitException(String error) {
        super(error);
    }

    public int getLimit() {
        return limit;
    }

    public UserOperateLimitException setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    @Override
    public String[] getData() {
        List<String> list = new ArrayList<>();
        list.add("limit: " + limit);
        return list.toArray(list.toArray(new String[0]));
    }

}
