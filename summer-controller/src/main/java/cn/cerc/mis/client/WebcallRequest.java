package cn.cerc.mis.client;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.tool.JsonTool;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class WebcallRequest {
    private static final Logger log = LoggerFactory.getLogger(WebcallRequest.class);

    private GetRequestValue getValue;
    private GetRequestValues getValues;
    private IHandle handle;

    public static interface GetRequestValue {
        String call(String key);
    }

    public WebcallRequest(IHandle handle) {
        this.handle = handle;
    }

    public void onGetValue(GetRequestValue getOne) {
        this.getValue = getOne;
    }

    public static interface GetRequestValues {
        String[] call(String key);
    }

    public void onGetValues(GetRequestValues getMany) {
        this.getValues = getMany;
    }

    public Object[] get(Method method) {
        int index = 0;
        Object[] args = new Object[method.getParameterCount()];
        for (Parameter param : method.getParameters()) {
            String key = param.getName();
            PathVariable name = param.getAnnotation(PathVariable.class);
            if (name != null && key.length() > 0)
                key = name.value();
            String value = getValue.call(key);
            log.debug("arg{}: {}", index, value);
            if ((param.getType() == List.class)) {
                Class<?> classType = null;
                Type type = method.getGenericParameterTypes()[index];
                // 当前参数类型
//                System.out.println("参数类型" + type);
                if (type instanceof ParameterizedType) {
                    ParameterizedType ptype = (ParameterizedType) type;
                    // 原始类型
//                    System.out.println("参数原始类型：" + ptype.getRawType());
                    // 获取对应泛型的类型
                    Type types = ptype.getActualTypeArguments()[0];
//                    System.out.println("泛型的类型: " + types.getTypeName());
                    if (types.getTypeName() == Integer.class.getName())
                        classType = Integer.class;
                    else if (types.getTypeName() == String.class.getName())
                        classType = String.class;
                    else
                        log.error(String.format("暂不支持的参数类型：%s", types.getTypeName()));
                }
                if (classType != null) {
                    List<Object> list = new ArrayList<Object>();
                    for (var item : getValues.call(key))
                        list.add(asParam(classType, item));
                    args[index] = list;
                }
            } else if (param.getType() == Map.class) {
                TypeFactory typeFactory = new ObjectMapper().getTypeFactory();
                MapType mapType = typeFactory.constructMapType(HashMap.class, String.class, String.class);
                Map<String, String> items = JsonTool.fromJsonElse(value, mapType, () -> new HashMap<String, String>());
                args[index] = items;
            } else if (param.getType() == IHandle.class) {
                args[index] = this.handle;
            } else if (param.getType() == DataSet.class) {
                args[index] = new DataSet().setJson(value);
            } else if (param.getType() == DataRow.class) {
                args[index] = new DataRow().setJson(value);
            } else
                args[index] = asParam(param.getType(), value);
            index++;
        }
        return args;
    }

    private Object asParam(Class<?> classType, String value) {
        if (classType == String.class)
            return value;
        else if (classType == Integer.class)
            return Integer.valueOf(value);
        else if (classType == int.class)
            return Integer.valueOf(value).intValue();
        else if (classType == Double.class)
            return Double.valueOf(value);
        else if (classType == double.class)
            return Double.valueOf(value).doubleValue();
        else if (classType == Long.class)
            return Long.valueOf(value);
        else if (classType == long.class)
            return Long.valueOf(value).longValue();
        else if (classType == Boolean.class)
            return Boolean.valueOf(value);
        else if (classType == boolean.class)
            return Boolean.valueOf(value).booleanValue();
        else if (classType == Datetime.class)
            return new Datetime(value);
        else {
            log.error(String.format("暂不支持的参数类型：%s", classType.getName()));
            return null;
        }
    }

    public static void execute(IHandle handle, HttpServletRequest request, HttpServletResponse response, Object service,
            String methodName) {
        try {
            for (var method : service.getClass().getMethods()) {
                if (methodName.equals(method.getName())) {
                    if (method.getModifiers() != Modifier.PUBLIC)
                        continue;
                    WebcallRequest data = new WebcallRequest(handle);
                    data.onGetValue(key -> request.getParameter(key));
                    data.onGetValues(key -> request.getParameterValues(key));
                    Object[] args = data.get(method);
                    Object obj = method.invoke(service, args);
                    if (obj instanceof DataSet dataOut)
                        response.getWriter().write(dataOut.json());
                    else
                        response.getWriter().write(JsonTool.toJson(new WebcallResult(true, "ok", obj)));
                    return;
                }
            }
            response.getWriter().write(JsonTool.toJson(new WebcallResult(false, "not find method: " + methodName)));
        } catch (IOException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            log.error(e.getMessage(), e);
            try {
                response.getWriter().write(JsonTool.toJson(new WebcallResult(false, e.getMessage())));
            } catch (IOException e1) {
                log.error(e.getMessage(), e1);
            }
        }
    }

}
