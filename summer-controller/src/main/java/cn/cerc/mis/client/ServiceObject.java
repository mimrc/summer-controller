package cn.cerc.mis.client;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.mis.core.IService;

@RequestMapping("/services")
public interface ServiceObject extends IService {

    DataSet call(IHandle handle);

    DataSet call(IHandle handle, @PathVariable("dataIn") DataRow headIn);

    DataSet call(IHandle handle, @PathVariable("dataIn") DataSet dataIn);

}
