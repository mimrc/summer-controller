package cn.cerc.mis.client;

import java.lang.reflect.Method;
import java.util.function.BiConsumer;

public interface WebcallEncoder {
    /**
     * 将args解码后调用 action
     * 
     * @param method
     * @param args
     * @param action
     */
    void execute(Method method, Object[] args, BiConsumer<String, String> action);

}
