package cn.cerc.mis.client;

import java.util.Optional;

import cn.cerc.db.core.IHandle;

public interface TokenSupplier {

    Optional<String> getToken(IHandle handle);

}
