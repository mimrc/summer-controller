package cn.cerc.mis.client;

public class RemoteServiceTimeoutException extends RuntimeException {

    private static final long serialVersionUID = 4322435677970324066L;

    public RemoteServiceTimeoutException(Throwable cause) {
        super(cause);
    }
}
