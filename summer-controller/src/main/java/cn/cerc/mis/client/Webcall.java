package cn.cerc.mis.client;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;

public class Webcall {
    private static final Logger log = LoggerFactory.getLogger(Webcall.class);
    private String path;
    private ServerSupplier serverSupply;
    private Consumer<WebcallExecutor> config;
    private Consumer<Throwable> onError;
    private WebcallExecutor executor = new WebcallExecutorDefault();

    public Webcall(ServerSupplier serverSupply) {
        this.serverSupply = serverSupply;
    }

    public Webcall config(Consumer<WebcallExecutor> config) {
        this.config = config;
        return this;
    }

    /**
     * 
     * @param executor 给此赋值后，将不再调用远程，而直接使用 mock.get 作为返回值
     * @return
     */
    public Webcall executor(WebcallExecutor executor) {
        this.executor = executor;
        return this;
    }

    public WebcallExecutor executor() {
        return executor;
    }

    private Object call(Method method, Object[] args) {
        try {
            if (args.length != method.getParameterCount())
                throw new RuntimeException(Lang.get(Webcall.class, 1, "参数定义的数目与调用的参数个数不符"));
            if (this.serverSupply == null)
                throw new RuntimeException(Lang.get(Webcall.class, 2, "serverSupply 不允许为空"));

            if (this.serverSupply instanceof IHandle server && args[0] instanceof IHandle source)
                server.setSession(source.getSession());

            String endpoint = this.serverSupply.getHost();
            if (endpoint == null || endpoint.endsWith("null"))
                throw new RuntimeException(Lang.get(Webcall.class, 3, "endpoint 不允许为空"));
            var url = endpoint + this.path + (this.path.endsWith("/") ? "" : ".") + method.getName();
            if (method.getDeclaringClass() == ServiceObject.class)
                url = endpoint + this.path;
            log.debug("url: " + url);
            if (this.config != null)
                this.config.accept(this.executor);
            return this.executor.get(this, url, method, args);
        } catch (Exception e) {
            Throwable cause = e.getCause() == null ? e : e.getCause();
            if (this.onError != null)
                this.onError.accept(cause);
            if (log.isDebugEnabled())
                SpringBean.printStackTrace(this.path);
            if (method.getReturnType() == DataSet.class)
                return new DataSet().setMessage(cause.getMessage());
            else
                throw new RuntimeException(cause.getMessage(), cause);
        }
    }

    /**
     * 
     * @param <T>
     * @param class1 此处传入的是一个接口对象
     * @return 返回实现这个接口的代理对象
     */
    @SuppressWarnings("unchecked")
    public <T> T target(Class<T> class1) {
        RequestMapping annotation = class1.getAnnotation(RequestMapping.class);
        String[] paths = annotation.value();
        if (paths.length != 1)
            throw new RuntimeException(String.format(Lang.get(Webcall.class, 4, "%s RequestMapping 注解赋值错误: 暂只支持一个路径"),
                    class1.getSimpleName()));
        this.path = paths[0];
        InvocationHandler handler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object result = call(method, args);
                return result;
            }
        };
        return (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[] { class1 },
                handler);
    }

    public ServiceObject target(String serviceCode) {
        var class1 = ServiceObject.class;
        RequestMapping annotation = class1.getAnnotation(RequestMapping.class);
        String[] paths = annotation.value();
        if (paths.length != 1)
            throw new RuntimeException(String.format(Lang.get(Webcall.class, 4, "%s RequestMapping 注解赋值错误: 暂只支持一个路径"),
                    class1.getSimpleName()));
        this.path = paths[0] + "/" + serviceCode;
        InvocationHandler handler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object result = call(method, args);
                return result;
            }
        };
        return (ServiceObject) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[] { class1 }, handler);
    }

    public static Webcall server(String endpoint) {
        return new Webcall(new ServerSimple(endpoint));
    }

    public static Webcall server(ServerSupplier class1) {
        return new Webcall(class1);
    }

}
