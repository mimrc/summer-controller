package cn.cerc.mis.client;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;

import cn.cerc.db.core.Curl;
import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.ISession;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.Utils;
import cn.cerc.mis.core.IService;

public class RemoteProxy implements WebcallExecutor {
    private static final Logger log = LoggerFactory.getLogger(RemoteProxy.class);
    private ServerSupplier server;
    private Class<?> class1;
    private ServiceExport serviceExport;

    public RemoteProxy server(Class<? extends ServerSupplier> serverSupplierClass) {
        this.server = SpringBean.get(serverSupplierClass);
        return this;
    }

    public RemoteProxy server(ServerSupplier serverSupplier) {
        this.server = serverSupplier;
        return this;
    }

    public <T> T target(Class<T> class1) {
        this.class1 = class1;
        if (!class1.isInterface())
            throw new RuntimeException("必须传递一个接口类");
        return Webcall.server(server).executor(this).target(class1);
    }

    public ServiceObject target(String service) {
        this.class1 = ServiceObject.class;
        return Webcall.server(server).executor(this).target(service);
    }

    @Override
    public Object get(Webcall webcall, String endpoint, Method method, Object[] args) {
        String token = null;
        IHandle handle = null;
        if (server instanceof IHandle temp)
            handle = temp;
        else if (args.length > 0 && args[0] instanceof IHandle temp)
            handle = temp;
        if (server instanceof TokenSupplier temp)
            token = temp.getToken(handle).orElse(null);
        // 开始调用
        try {
            Curl curl = new Curl();
            if (!Utils.isEmpty(token))
                curl.put(ISession.TOKEN, token);

            // 处理输入
            if (IService.class.isAssignableFrom(this.class1)) {
                DataSet dataIn = RemoteProxy.buildDataIn(method, args);
                if (dataIn != null)
                    curl.add("dataIn", dataIn.json());
                if (serviceExport != null)
                    serviceExport.init(dataIn);
            } else {
                var keys = new ArrayList<String>();
                var values = new ArrayList<String>();
                WebcallEncoderDefault encoder = new WebcallEncoderDefault();
                encoder.execute(method, args, (key, value) -> {
                    if (!key.equals(ISession.TOKEN)) {
                        keys.add(key);
                        values.add(value);
                    }
                });
                for (int i = 0; i < keys.size(); i++) {
                    var key = keys.get(i);
                    curl.add(key, values.get(i));
                }
            }

            // 执行
            String response = curl.doPost(endpoint);
            // 处理输出
            WebcallDecoderDefault decoder = new WebcallDecoderDefault();
            return decoder.get(method.getReturnType(), response);
        } catch (Exception e) {
            log.error("{} 调用异常 {}", endpoint, e.getMessage(), e);
            if (method.getReturnType() == DataSet.class)
                return new DataSet().setMessage("remote error: " + e.getMessage());
            else
                throw new RuntimeException(e);
        }
    }

    /*
     * 此函数对多线程存在问题
     */
    @Deprecated
    public RemoteProxy export(ServiceExport serviceExport) {
        this.serviceExport = serviceExport;
        return this;
    }

    /**
     * 检查参数长度、类型，并返回dataIn
     * 
     * @param args
     * @return
     */
    public static DataSet buildDataIn(Method method, Object[] args) {
        if (args.length == 0 || !(args[0] instanceof IHandle))
            throw new RuntimeException("参数数量错误，最少必须有1个，且第1个必须为 IHandle 类型");
        if (args.length == 1 && (args[0] instanceof IHandle))
            return new DataSet();
        else if (args.length == 2 && args[1] instanceof DataSet dataIn)
            return dataIn;
        else if (args.length == 2 && args[1] instanceof DataRow headIn)
            return headIn.toDataSet();
        else if (args.length == 2 && args[1] == null)
            return null;
        else if (args.length > 1 && args[1] != null) {
            DataRow row = new DataRow();
            for (var i = 1; i < args.length; i++) {
                if (args[i] != null) {
                    var param = method.getParameters()[i];
                    PathVariable anno = param.getAnnotation(PathVariable.class);
                    if (anno == null)
                        throw new RuntimeException("参数必须增加 PathVariable 注解");
                    var arg = args[i];
                    row.setValue(anno.value(), arg);
                }
            }
            return row.toDataSet();
        } else {
            throw new RuntimeException("暂不支持的参数数量与类型");
        }
    }

}
