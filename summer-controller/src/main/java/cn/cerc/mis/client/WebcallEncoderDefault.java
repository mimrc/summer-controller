package cn.cerc.mis.client;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.tool.JsonTool;

public class WebcallEncoderDefault implements WebcallEncoder {
    private static final Logger log = LoggerFactory.getLogger(WebcallEncoderDefault.class);

    @Override
    public void execute(Method method, Object[] args, BiConsumer<String, String> action) {
        int current = 0;
        for (Parameter param : method.getParameters()) {
            // 若定义的参数第1个为IHandle类型，则予以跳过，将token赋值到参数中
            if (current == 0 && IHandle.class.isAssignableFrom(param.getType())) {
                IHandle handle = (IHandle) args[current];
                if (handle != null)
                    action.accept("token", handle.getSession().getToken());
                current++;
                continue;
            }
            PathVariable pathVar = param.getDeclaredAnnotation(PathVariable.class);
            String key = param.getName();
            if (pathVar != null)
                key = pathVar.value();
            var item = args[current];
            if (item instanceof List list) {
                for (var item1 : list) {
                    if (item1 != null) {
                        String value = String.valueOf(item1);
                        log.debug(String.format("key=%s, value=%s", key, value));
                        action.accept(key, value);
                    }
                }
            } else if (item instanceof Map map) {
                String value = JsonTool.toJson(map);
                log.debug(String.format("key=%s, value=%s", key, value));
                action.accept(key, value);
            } else {
                var item1 = args[current];
                if (item1 != null) {
                    String value = String.valueOf(item1);
                    log.debug(String.format("key=%s, value=%s", key, value));
                    action.accept(key, value);
                }
            }
            current++;
        }
    }

}
