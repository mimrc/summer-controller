package cn.cerc.mis.client;

import java.util.Objects;
import java.util.function.Consumer;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.ISession;

public class ServiceProxy implements IHandle {
    private DataSet dataIn;
    private DataSet dataOut;
    private ISession session;

    public final boolean isOk() {
        Objects.requireNonNull(dataOut);
        return dataOut.state() > 0;
    }

    public boolean isOkElse(Consumer<String> action) {
        if (isOk())
            return true;
        if (action != null)
            action.accept(dataOut.message());
        return false;
    }

    public final boolean isOkElseThrow() {
        dataOut.elseThrow();
        return true;
    }

    public final boolean isFail() {
        Objects.requireNonNull(dataOut);
        return dataOut.state() <= 0;
    }

    public final boolean isFail(Consumer<String> action) {
        Objects.requireNonNull(dataOut);
        boolean result = dataOut.state() <= 0;
        if (result && action != null)
            action.accept(dataOut.message());
        return result;
    }

    /**
     * 请改使用 elseThrow 函数
     * 
     * @return
     */
    @Deprecated
    public final DataSet getDataOutElseThrow() {
        return elseThrow();
    }

    public final DataSet elseThrow() {
        Objects.requireNonNull(dataOut);
        return dataOut.elseThrow();
    }

    public final DataRow getHeadOutElseThrow() {
        Objects.requireNonNull(dataOut);
        dataOut.elseThrow();
        return dataOut.head();
    }

    public String message() {
        return dataOut().message();
    }

    @Override
    public ISession getSession() {
        return session;
    }

    @Override
    public void setSession(ISession session) {
        this.session = session;
    }

    public final DataSet dataOut() {
        if (this.dataOut == null)
            this.dataOut = new DataSet();
        return dataOut;
    }

    protected void setDataOut(DataSet dataOut) {
        this.dataOut = dataOut;
    }

    public final DataSet dataIn() {
        if (this.dataIn == null)
            this.dataIn = new DataSet();
        return dataIn;
    }

    protected void setDataIn(DataSet dataIn) {
        this.dataIn = dataIn;
    }

}
