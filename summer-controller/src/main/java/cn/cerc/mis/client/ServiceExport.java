package cn.cerc.mis.client;

import org.springframework.web.bind.annotation.RequestMapping;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.mis.core.SystemBuffer;
import cn.cerc.mis.other.MemoryBuffer;

public class ServiceExport {
    private IHandle handle;
    private String exportKey;
    private String service;

    /**
     * 请改为直接调用
     * 
     * @param handle
     * @param dataIn
     * @return
     */
    @Deprecated
    public static String build(IHandle handle, DataSet dataIn) {
        return new ServiceExport(handle, dataIn).getExportKey();
    }

    public ServiceExport(IHandle handle) {
        this.handle = handle;
    }

    public ServiceExport(IHandle handle, DataSet dataIn) {
        this.handle = handle;
        this.init(dataIn);
    }

    public ServiceExport(IHandle handle, DataRow headIn) {
        this.handle = handle;
        this.init(headIn.toDataSet());
    }

    public String init(DataSet dataIn) {
        if (dataIn == null)
            throw new RuntimeException("export dataIn can not be null.");
        this.exportKey = String.valueOf(System.currentTimeMillis());
        try (MemoryBuffer buff = new MemoryBuffer(SystemBuffer.User.ExportKey, handle.getUserCode(), exportKey)) {
            buff.setValue("data", dataIn.json());
        }
        return exportKey;
    }

    public String getExportKey() {
        return this.exportKey;
    }

    public String getService() {
        return service;
    }

    /**
     * 请改使用 getService
     * 
     * @return
     */
    @Deprecated
    public String id() {
        return this.getService();
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setService(Class<?> class1) {
        if (!class1.isInterface())
            throw new RuntimeException(Lang.as("仅支持接口类"));
        RequestMapping annotation = class1.getAnnotation(RequestMapping.class);
        if (annotation == null)
            throw new RuntimeException(Lang.as("必须定义 RequestMapping 注解"));
        if (annotation.value().length != 1)
            throw new RuntimeException(Lang.as("当前仅支持单路径"));
        var path = annotation.value()[0].split("/");
        this.service = path[path.length - 1];
    }

}
