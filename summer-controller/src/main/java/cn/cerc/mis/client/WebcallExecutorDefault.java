package cn.cerc.mis.client;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.Curl;

public class WebcallExecutorDefault implements WebcallExecutor {
    private static final Logger log = LoggerFactory.getLogger(WebcallExecutorDefault.class);
    private WebcallEncoder encoder = new WebcallEncoderDefault();
    private WebcallDecoder decoder = new WebcallDecoderDefault();
    /**
     * 读取数据超时，默认10秒
     */
    private int readTimeout = 10000;

    public WebcallEncoder encoder() {
        return encoder;
    }

    public WebcallDecoder decoder() {
        return decoder;
    }

    @Override
    public void encoder(WebcallEncoder encoder) {
        if (encoder == null)
            throw new RuntimeException("encoder is null");
        this.encoder = encoder;
    }

    @Override
    public void decoder(WebcallDecoder decoder) {
        if (decoder == null)
            throw new RuntimeException("decoder is null");
        this.decoder = decoder;
    }

    @Override
    public int getReadTimeout() {
        return this.readTimeout;
    }

    /**
     * 
     * @param readTimeout 设置调用超时时间, 默认 10000 毫秒
     */
    @Override
    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    @Override
    public Object get(Webcall webcall, String url, Method method, Object[] args) {
        var keys = new ArrayList<String>();
        var values = new ArrayList<String>();
        encoder.execute(method, args, (key, value) -> {
            keys.add(key);
            values.add(value);
        });
        // 执行请求
        var curl = new Curl();
        curl.setReadTimeout(this.getReadTimeout());
        for (int i = 0; i < keys.size(); i++) {
            var key = keys.get(i);
            curl.add(key, values.get(i));
        }
        String response;
        try {
            response = curl.doPost(url);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        log.debug("response: " + response);
        return this.decoder.get(method.getReturnType(), response);
    }

}
