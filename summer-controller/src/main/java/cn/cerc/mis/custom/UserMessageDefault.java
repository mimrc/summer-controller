package cn.cerc.mis.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.ISession;
import cn.cerc.db.mysql.MysqlQuery;
import cn.cerc.db.redis.Redis;
import cn.cerc.mis.core.ISystemTable;
import cn.cerc.mis.core.UserMessage;
import cn.cerc.mis.message.MessageLevel;
import cn.cerc.mis.message.MessageProcess;
import cn.cerc.mis.message.MessageRecord;
import cn.cerc.mis.other.MemoryBuffer;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserMessageDefault implements UserMessage {

    @Autowired
    private ISystemTable systemTable;
    private ISession session;

    @Override
    public String send(IHandle handle, String corpNo, String userCode, MessageLevel level, String subject,
            String content, MessageProcess process, String UIClass) {
        // 若为异步任务消息请求
        if (level == MessageLevel.Service) {
            // 若已存在同一公司别同一种回算请求在排队或者执行中，则不重复插入回算请求
            MysqlQuery ds2 = new MysqlQuery(handle);
            ds2.setMaximum(1);
            ds2.add("select UID_ from %s ", systemTable.getUserMessages());
            ds2.add("where CorpNo_='%s' ", corpNo);
            ds2.add("and Subject_='%s' ", subject);
            ds2.add("and Level_=4 and (Process_ = 1 or Process_=2)");
            ds2.open();
            if (ds2.size() > 0) {
                // 返回消息的编号
                return ds2.getString("UID_");
            }
        }
        
        MysqlQuery cdsMsg = new MysqlQuery(handle);
        cdsMsg.add("select * from %s", systemTable.getUserMessages());
        cdsMsg.setMaximum(0);
        cdsMsg.open();

        // 保存到数据库
        cdsMsg.append();
        cdsMsg.setValue("CorpNo_", corpNo);
        cdsMsg.setValue("UserCode_", userCode);
        cdsMsg.setValue("Level_", level.ordinal());
        cdsMsg.setValue("Subject_", subject);
        if (content.length() > 0) {
            cdsMsg.setValue("Content_", content);
        }
        cdsMsg.setValue("AppUser_", session.getUserCode());
        cdsMsg.setValue("AppDate_", new Datetime());
        // 日志类消息默认为已读
        cdsMsg.setValue("Status_", level == MessageLevel.Logger ? 1 : 0);
        cdsMsg.setValue("Process_", process == null ? 0 : process.ordinal());
        cdsMsg.setValue("Final_", false);
        cdsMsg.post();

        // 清除缓存
        String buffKey = MemoryBuffer.buildObjectKey(MessageRecord.class, corpNo + "." + userCode);
        Redis.delete(buffKey);

        // 返回消息的编号
        return cdsMsg.getString("UID_");
    }

}
