package cn.cerc.mis.language;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.cerc.db.core.IHandle;

@RequestMapping("/services/ApiTranslate")
public interface ApiTranslate {

    List<String> as(IHandle handle, @PathVariable("source") String source, @PathVariable("target") String target,
            @PathVariable("text") List<String> texts);

    Map<String, String> get(IHandle handle, @PathVariable("group") String group, @PathVariable("source") String source,
            @PathVariable("target") String target, @PathVariable("items") Map<String, String> items);

    Map<String, String> load(IHandle handle, @PathVariable("group") String group,
            @PathVariable("target") String target);

}
