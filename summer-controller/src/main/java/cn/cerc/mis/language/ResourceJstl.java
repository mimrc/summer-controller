package cn.cerc.mis.language;

import java.io.IOException;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.PageContext;
import jakarta.servlet.jsp.tagext.JspFragment;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

public class ResourceJstl extends SimpleTagSupport {
    private static final Logger log = LoggerFactory.getLogger(ResourceJstl.class);
    private String toId = null;

    @Override
    public void doTag() throws JspException, IOException {
        JspFragment jf = this.getJspBody();
        String text = "";
        try {
            HttpServletRequest request = (HttpServletRequest) ((PageContext) this.getJspContext()).getRequest();
            try {
                text = getString(request, jf);
            } catch (Exception e) {
                String uri = request.getRequestURI();
                log.error("key is empty，uri：" + uri);
                text = "file error";
            }
        } finally {
            if (this.toId == null) {
                this.getJspContext().getOut().write(text);
            } else {
                this.getJspContext().setAttribute(this.toId, text);
            }
            super.doTag();
        }
    }

    private String getString(HttpServletRequest request, JspFragment jf) throws JspException, IOException {
        IHandle handle = (IHandle) request.getAttribute("myappHandle");
        if (handle == null) {
            log.error("handle is null");
            return "handle is null";
        }

        StringWriter sw = new StringWriter();
        jf.invoke(sw);

        // 取得到值，则直接返回
        return Lang.as(sw.toString());
    }

    public void setToId(String toId) {
        this.toId = toId;
    }
}
