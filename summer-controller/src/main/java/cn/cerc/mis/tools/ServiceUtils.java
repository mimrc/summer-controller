package cn.cerc.mis.tools;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.MD5;
import cn.cerc.db.core.Utils;
import cn.cerc.db.core.Variant;
import cn.cerc.db.log.KnowallLog;
import cn.cerc.db.redis.Redis;
import cn.cerc.db.redis.SingleLocker;
import cn.cerc.mis.core.IService;
import cn.cerc.mis.core.IVuiReport;
import cn.cerc.mis.core.ServiceCache;
import cn.cerc.mis.core.ServiceMethod;
import cn.cerc.mis.core.ServiceNameAwareImpl;
import cn.cerc.mis.core.ServiceState;
import cn.cerc.mis.core.SystemBuffer;
import cn.cerc.mis.other.MemoryBuffer;

public class ServiceUtils {
    private static Logger log = LoggerFactory.getLogger(IService.class);

    public static DataSet call(IService sender, IHandle handle, DataSet dataIn, Variant function)
            throws IllegalAccessException, InvocationTargetException, DataException, RuntimeException {
        String redisKey = null;
        ServiceCache cacheConfig = sender.getClass().getAnnotation(ServiceCache.class);
        if (cacheConfig != null && cacheConfig.expire() > 0) {
            String level = switch (cacheConfig.level()) {
            case user -> handle.getUserCode();
            case corp -> handle.getCorpNo();
            case system -> "";
            default -> handle.getSession().getToken();
            };
            String md5 = MD5.get(level + function.getString() + dataIn.json());
            int prefix = MemoryBuffer.prefix(SystemBuffer.Service.Cache);
            redisKey = String.join(".", String.valueOf(prefix), sender.getClass().getSimpleName(), md5);
            try (Redis redis = new Redis()) {
                String json = redis.get(redisKey);
                if (Utils.isNotEmpty(json)) {
                    return new DataSet().setJson(json);
                }
            }
        } else if (sender instanceof IVuiReport) {
            // 每小时汇报1次此种情况
            if (SingleLocker.lock(sender, sender.getClass().getSimpleName(), 3600)) {
                var klog = new KnowallLog().setLevel("warn").setType(ServiceCache.class.getSimpleName());
                klog.setMessage(sender.getClass().getSimpleName() + ": 没有加上缓存注解，会导致性能问题！").post();
            }
        }

        if (function == null || Utils.isEmpty(function.getString()))
            return new DataSet().setMessage("function is null");
        if ("_call".equals(function.getString()))
            return new DataSet().setMessage("function is call");

        String funcCode = function.getString();
        ServiceMethod sm = ServiceMethod.build(sender.getClass(), funcCode);
        if (sm == null) {
            // 支持 PartVariant
            for (var method : sender.getClass().getMethods()) {
                if (funcCode.equals(method.getName()) && method.getParameters().length > 0
                        && method.getReturnType() == DataSet.class) {
                    var items = new Object[method.getParameterCount()];
                    items[0] = handle;
                    for (var i = 1; i < method.getParameterCount(); i++) {
                        var param = method.getParameters()[i];
                        PathVariable anno = param.getAnnotation(PathVariable.class);
                        if (anno != null) {
                            if (param.getType() == String.class)
                                items[i] = dataIn.head().getString(anno.value());
                            else if (param.getType() == int.class)
                                items[i] = dataIn.head().getInt(anno.value());
                            else if (param.getType() == Integer.class)
                                items[i] = Integer.valueOf(dataIn.head().getInt(anno.value()));
                            else if (param.getType() == long.class)
                                items[i] = dataIn.head().getLong(anno.value());
                            else if (param.getType() == Long.class)
                                items[i] = Long.valueOf(dataIn.head().getLong(anno.value()));
                            else if (param.getType() == Datetime.class)
                                items[i] = dataIn.head().getDatetime(anno.value());
                            else if (param.getType() == boolean.class)
                                items[i] = dataIn.head().getBoolean(anno.value());
                            else if (param.getType() == Boolean.class)
                                items[i] = Boolean.valueOf(dataIn.head().getBoolean(anno.value()));
                            else
                                throw new RuntimeException("不支持的参数类型：" + param.getType().getName());
                        }
                    }
                    return (DataSet) method.invoke(sender, items);
                }
            }
            DataSet dataOut = new DataSet();
            dataOut.setMessage(String.format("%s.%s not find！", sender.getClass().getName(), funcCode));
            return dataOut.setState(ServiceState.NOT_FIND_SERVICE);
        }

        // 执行具体的服务函数
        if (sender instanceof ServiceNameAwareImpl service) {
            String key = function.key();
            if (key != null && key.endsWith(".execute"))
                function.setKey(key.substring(0, key.lastIndexOf(".execute")));
            service.setServiceId(function);
        }
        DataSet dataSet = sm.call(sender, handle, dataIn);
        if (cacheConfig != null && cacheConfig.expire() > 0) {
            try (Redis redis = new Redis()) {
                long seconds = cacheConfig.expire();
                if (seconds < 3) {
                    seconds = 3L;
                    log.warn("{} 服务缓存过期时间不允许小于3秒，强制改为3秒", sender.getClass().getSimpleName());
                }
                if (seconds > TimeUnit.HOURS.toSeconds(24)) {
                    seconds = TimeUnit.HOURS.toSeconds(24);
                    log.warn("{} 服务缓存过期时间不允许大于24小时，强制改为24小时", sender.getClass().getSimpleName());
                }
                redis.setex(redisKey, seconds, dataSet.json());
            }
        }
        return dataSet;

    }

}
