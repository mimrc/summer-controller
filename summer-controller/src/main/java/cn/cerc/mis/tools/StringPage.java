package cn.cerc.mis.tools;

import java.io.IOException;

import cn.cerc.mis.core.IForm;
import cn.cerc.mis.core.IPage;
import jakarta.servlet.ServletException;

public class StringPage implements IPage {

    private Object origin;
    private IForm form;
    private String context;

    public StringPage(IForm form, String context) {
        this.origin = form;
        this.form = form;
        this.context = context;
    }

    @Override
    public Object setOrigin(Object origin) {
        this.origin = origin;
        return this;
    }

    @Override
    public Object getOrigin() {
        return origin;
    }

    @Override
    public IForm getForm() {
        return form;
    }

    @Override
    public String execute() throws ServletException, IOException {
        return context;
    }

}
