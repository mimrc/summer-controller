package cn.cerc.mis.cdn;

import cn.cerc.db.core.ConfigReader;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.Utils;

public class CDN {
    // 启用内容网络分发
    @Deprecated
    public static final String OSS_CDN_ENABLE = "oss.cdn.enable";
    // 浏览器缓存版本号
    public static final String BROWSER_CACHE_VERSION = "browser.cache.version";

    public static String get(String file) {
        ConfigReader config = ConfigReader.instance();
        return file + "?v=" + config.getProperty(BROWSER_CACHE_VERSION, "1.0.0.0");
    }

    /**
     * TODO 改为从zookeeper读取配置
     */
    @Deprecated
    public static String getSite() {
        ConfigReader config = ConfigReader.instance();
        String site = config.getProperty("cdn.site", "");
        if (Utils.isEmpty(site))
            throw new RuntimeException(Lang.get(CDN.class, 1, "CDN 地址没有配置"));
        return site;
    }

}
