package cn.cerc.mis.excel.input;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.mis.core.DiskFileItem;
import cn.cerc.mis.core.IForm;
import cn.cerc.mis.core.MultipartFiles;
import jakarta.servlet.http.HttpServletRequest;

public class ImportExcelFile {
    private HttpServletRequest request;
    private DataSet dataSet = new DataSet();
    private List<DiskFileItem> uploadFiles;

    public int init(IForm form) {
        // 处理文件上传
        uploadFiles = MultipartFiles.get(form);
        if (uploadFiles.isEmpty())
            return 0;

        for (int i = 0; i < uploadFiles.size(); i++) {
            DiskFileItem fileItem = uploadFiles.get(i);
            if (fileItem.isFormField()) {
                // 普通数据
                String val = Arrays.stream(fileItem.getString().split(",")).distinct().collect(Collectors.joining(","));
                dataSet.head().setValue(fileItem.getFieldName(), val);
            } else {
                // 文件数据
                if (fileItem.getSize() > 0) {
                    DataRow rs = dataSet.append().current();
                    rs.setValue("_FieldNo", i);
                    rs.setValue("_FieldName", fileItem.getFieldName());
                    rs.setValue("_ContentType", fileItem.getContentType());
                    rs.setValue("_FileName", fileItem.getName());
                    rs.setValue("_FileSize", fileItem.getSize());
                }
            }
        }
        dataSet.first();
        return dataSet.size();
    }

    public DiskFileItem getFile(DataRow record) {
        int fileNo = record.getInt("_FieldNo");
        return uploadFiles.get(fileNo);
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public ImportExcelFile setRequest(HttpServletRequest request) {
        this.request = request;
        return this;
    }

    public DataSet dataSet() {
        return dataSet;
    }

    @Deprecated
    public final DataSet getDataSet() {
        return dataSet();
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }
}
