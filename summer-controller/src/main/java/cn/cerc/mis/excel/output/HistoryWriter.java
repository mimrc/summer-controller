package cn.cerc.mis.excel.output;

import cn.cerc.db.core.IHandle;

public interface HistoryWriter {

    void finish(IHandle handle, ExcelTemplate template);

}
