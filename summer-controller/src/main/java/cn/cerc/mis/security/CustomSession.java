package cn.cerc.mis.security;

import java.util.HashMap;
import java.util.Map;

import cn.cerc.db.core.ISession;
import cn.cerc.db.core.Lang;
import cn.cerc.mis.core.Application;

public class CustomSession implements ISession {
    private final Map<String, Object> properties = new HashMap<>();
    public static final String machineCode = "T1000";

    public CustomSession() {
        super();
        properties.put(ISession.CORP_NO, "");
        properties.put(ISession.USER_CODE, "");
        properties.put(ISession.USER_NAME, "");
        properties.put(ISession.LANGUAGE_ID, Lang.id());
        properties.put(Application.ClientIP, "0.0.0.0");
        properties.put(Application.ProxyUsers, "");
    }

    @Override
    public final void setProperty(String key, Object value) {
        if (ISession.TOKEN.equals(key)) {
            if ("{}".equals(value)) {
                properties.put(key, null);
            } else {
                if (value == null || "".equals(value)) {
                    properties.remove(ISession.TOKEN);
                    properties.remove(ISession.CORP_NO);
                    properties.remove(ISession.USER_CODE);
                    properties.remove(ISession.USER_NAME);
                    properties.remove(Application.ProxyUsers);
                } else {
                    properties.put(key, value);
                }
            }
            return;
        }
        ISession.super.setProperty(key, value);
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

}
