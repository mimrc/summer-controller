package cn.cerc.mis.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.Handle;
import cn.cerc.db.core.ISession;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.Utils;
import cn.cerc.db.mysql.MysqlConfig;
import cn.cerc.db.mysql.MysqlServer;
import cn.cerc.db.redis.Redis;
import cn.cerc.db.redis.RedisRecord;
import cn.cerc.mis.core.Application;
import cn.cerc.mis.core.SystemBuffer;
import cn.cerc.mis.other.MemoryBuffer;

public class SessionFactory {
    private static final Logger log = LoggerFactory.getLogger(SessionFactory.class);

    public static ISession get() {
        return new CustomSession();
    }

    public static ISession getByToken(String token) {
        ISession session = new CustomSession();
        SessionFactory.loadToken(session, token);
        return session;
    }

    public static boolean loadToken(ISession session, String token) {
        ISecurityService security = Application.getBean(ISecurityService.class);
        if (security == null)
            return false;

        if (!security.initSession(session, token))
            return false;

        if (Utils.isEmpty(token))
            return false;

        String key = MemoryBuffer.buildKey(SystemBuffer.Token.UserInfoHash, token);
        try (Redis redis = new Redis()) {
            String value = redis.hget(key, SystemBuffer.UserObject.Permissions.name());
            if (value == null) {
                value = security.getPermissions(session);
                if (value != null) {
                    redis.hset(key, SystemBuffer.UserObject.Permissions.name(), value);
                    redis.expire(key, RedisRecord.TIMEOUT);
                } else {
                    redis.hdel(key, SystemBuffer.UserObject.Permissions.name());
                }
            }
            session.setProperty(ISession.USER_PERMISSIONS, value);
            log.debug("{}.{}[permissions]={}", session.getCorpNo(), session.getUserCode(), value);
        }
        SessionFactory.switchCustomDatabase(session);
        return true;
    }

    public static void loadTokenBySystemUser(ISession session) {
        ISecurityService security = Application.getBean(ISecurityService.class);
        if (security != null) {
            String token = security.getSystemUserToken(new Handle(session), session.getCorpNo(),
                    CustomSession.machineCode);
            SessionFactory.loadToken(session, token);
        }
    }

    /**
     * 检测是否存在帐套专用数据库，若有则予以切换
     * 
     * @return 切换成功否
     */
    public static boolean switchCustomDatabase(ISession session) {
        // 检测是否存在帐套专用数据库，若有则予以切换
        var curServer = session.getProperty(MysqlServer.SessionId, MysqlServer.class);
        MysqlConfig config = curServer.getMysqlConfig();
        if (!config.exists(session.getCorpNo()))
            return false;
        // 切换到新的配置
        MysqlServer newServer = SpringBean.get(MysqlServer.class);
        newServer.setMysqlConfig(session.getCorpNo());
        session.setProperty(MysqlServer.SessionId, newServer);
        curServer.close(); // 关闭原来的 mysql 数据库连接
        return true;
    }

    /**
     * 更改 session 中的帐套代码与用户代码
     * 
     * @param session
     * @param corpNo   指定帐套的代码
     * @param userCode 指定用户的代码
     */
    public static void switchCorpUser(ISession session, String corpNo, String userCode) {
        session.setProperty(ISession.CORP_NO, corpNo);
        session.setProperty(ISession.USER_CODE, userCode);
        SessionFactory.switchCustomDatabase(session);
    }

}
