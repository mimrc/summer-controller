package cn.cerc.mis.other;

public class PageNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -5815555198624305910L;

    public PageNotFoundException(String message) {
        super(message);
    }

}
