package cn.cerc.mis.book;

import cn.cerc.db.core.ClassResource;
import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.Variant;
import cn.cerc.mis.SummerMIS;

/**
 * 帐本管理器
 */
public interface BatchManager extends IHandle {
    ClassResource res = new ClassResource(BatchManager.class, SummerMIS.ID);

    Variant getOption(String key);

    // 是否为批处理模式（月度回算 or 单据过帐)
    boolean isBatchMode();

    // 设置过帐日期范围，在用于回算时令 force=false, 在用于查询或超过2个月的记录要进行过帐时，令fore=true
    void setDateRange(Datetime beginDate, Datetime endDate, boolean forceExecute);

    // 取得回算年月
    default String getBookMonth() {
        Datetime dateFrom = getDateFrom();
        if (dateFrom == null) {
            throw new RuntimeException(Lang.get(BatchManager.class, 1, "帐本年月不允许为空！"));
        }
        return dateFrom.getYearMonth();
    }

    default void setBookMonth(String beginYearMonth) {
        // 传入日期年月大于当前年月则默认为当前年月
        if (beginYearMonth.compareTo(new Datetime().getYearMonth()) > 0) {
            beginYearMonth = new Datetime().getYearMonth();
        }
        setDateRange(new Datetime(beginYearMonth), new Datetime(), false);
    }

    // 是否预览变更而不保存
    boolean isPreviewUpdate();

    // 是否指定料号回算
    String getPartCode();

    // 取得开始日期
    Datetime getDateFrom();

    // 取得结束日期
    Datetime getDateTo();

    // 取得期初年月
    String getInitMonth();

}
