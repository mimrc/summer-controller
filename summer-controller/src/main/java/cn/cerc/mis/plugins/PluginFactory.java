package cn.cerc.mis.plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;

public class PluginFactory {
    private static final Logger log = LoggerFactory.getLogger(PluginFactory.class);

    public static <T extends Plugin> Optional<T> getPlugin(Object owner, Class<T> requiredType) {
        var list = getPlugins(owner, requiredType);
        if (list.isEmpty())
            return Optional.empty();
        else if (list.size() > 1) {
            List<String> items = new ArrayList<>();
            for (var plugins : list)
                items.add(plugins.getClass().getSimpleName());
            var errorMessage = String.format(Lang.get(PluginFactory.class, 1, "接口 %s 出现重复的实现类：%s"),
                    requiredType.getClass().getSimpleName(), String.join(",", items));
            log.error(errorMessage);
            throw new RuntimeException(errorMessage);
        }
        return Optional.of(list.get(0));
    }

    public static <T extends Plugin> List<T> getPlugins(Object owner, Class<T> requiredType) {
        Objects.requireNonNull(owner);
        var list = new ArrayList<T>();
        ApplicationContext context = SpringBean.context();
        if (context != null) {
            for (var beanId : context.getBeanNamesForType(requiredType)) {
                T bean = context.getBean(beanId, requiredType);
                if (bean.setOwner(owner))
                    list.add(bean);
            }
        }
        return list;
    }

    public static boolean enabled(Object owner, Class<? extends Plugin> class1) {
        return getPlugins(owner, class1).size() > 0;
    }

}
