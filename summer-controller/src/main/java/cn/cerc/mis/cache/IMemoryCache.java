package cn.cerc.mis.cache;

import org.springframework.beans.factory.BeanNameAware;

import cn.cerc.db.core.IHandle;

/**
 * spring single scope cache
 */
public interface IMemoryCache extends BeanNameAware {
    final String Clear = "clear";

    void resetCache(IHandle handle, CacheResetMode resetType, String param);

    String getBeanName();

    default void clear() {
        MemoryListener.refresh(this.getClass(), Clear);
    }

    default void clear(String param) {
        MemoryListener.refresh(this.getClass(), param);
    }

}
