package cn.cerc.mis.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.ConfigReader;
import cn.cerc.db.redis.RedisClient;
import cn.cerc.mis.core.BasicHandle;
import jakarta.annotation.Resource;

@Component
public class MemoryCacheEvent implements ApplicationRunner, MessageListener, ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(MemoryCacheEvent.class);
    private RedisClient redis;
    private ApplicationContext context;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        redis.subscribe(this, MemoryListener.CacheChannel);
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }

    @Resource
    public void setRedisClient(RedisClient redis) {
        this.redis = redis;
    }

    @Override
    public void onMessage(Message value, byte[] pattern) {
        String message = value.toString();
        try {
            String[] args = message.split(":");
            String beanId = args[0];
            if (context.containsBean(beanId)) {
                if (context.isSingleton(beanId)) {
                    String param = null;
                    if (args.length > 1)
                        param = message.substring(beanId.length() + 1);
                    log.debug("{}.resetCache:{}", beanId, param);
                    IMemoryCache bean = context.getBean(beanId, IMemoryCache.class);
                    try (BasicHandle handle = new BasicHandle()) {
                        bean.resetCache(handle, CacheResetMode.Update, param);
                    }
                } else {
                    log.info("{} 中对象 {} 非单例模式，不支持通知刷新", ConfigReader.instance().getAppName(), beanId);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
