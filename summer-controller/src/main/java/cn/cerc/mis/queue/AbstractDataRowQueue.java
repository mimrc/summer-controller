package cn.cerc.mis.queue;

import java.util.Objects;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.DataCell;
import cn.cerc.db.core.DataException;
import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.StateMessage;
import cn.cerc.db.core.Utils;
import cn.cerc.db.queue.AbstractQueue;
import cn.cerc.db.queue.MessageData;
import cn.cerc.db.queue.MessageManager;
import cn.cerc.db.queue.MessageProps;
import cn.cerc.db.queue.QueueItem;
import cn.cerc.db.queue.rabbitmq.SimpleGroup;
import cn.cerc.mis.client.CorpConfigImpl;
import cn.cerc.mis.client.RemoteService;
import cn.cerc.mis.security.SessionFactory;

public abstract class AbstractDataRowQueue extends AbstractQueue {
    private static final Logger log = LoggerFactory.getLogger(AbstractDataRowQueue.class);

    public String pushToLocal(IHandle handle, DataRow data) {
        SimpleGroup group = new SimpleGroup(handle);
        var result = group.addItem(this.getToLocal(handle, data));
        group.start();
        return result.getMsgId();
    }

    public String pushToRemote(IHandle handle, CorpConfigImpl config, DataRow dataRow) throws DataException {
        SimpleGroup group = new SimpleGroup(handle);
        var result = group.addItem(this.getToRemote(handle, config, dataRow));
        group.start();
        return result.getMsgId();
    }

    public QueueItem getToLocal(IHandle handle, DataRow dataRow) {
        if (dataRow.hasValue("token"))
            log.warn("{}.appendToLocal 代码编写不符合规范，请予改进", this.getClass().getName());
        else
            dataRow.setValue("token", handle.getSession().getToken());
        if (!dataRow.hasValue("corp_no_"))
            dataRow.setValue("corp_no_", handle.getSession().getCorpNo());
        if (!dataRow.hasValue("user_code_"))
            dataRow.setValue("user_code_", handle.getSession().getUserCode());

        return new QueueItem(this, dataRow.json());
    }

    private QueueItem getToRemote(IHandle handle, CorpConfigImpl config, DataRow dataRow) throws DataException {
        Objects.requireNonNull(config);
        if (!Utils.isEmpty(config.getCorpNo())) {
            var serviceConfig = RemoteService.getServerConfig(SpringBean.context());
            if (serviceConfig.isPresent()) {
                var remoteToken = serviceConfig.get().getToken(handle, config.getCorpNo());
                if (remoteToken.isPresent())
                    dataRow.setValue("token", remoteToken.get());
            }
        }

        if (!dataRow.hasValue("corp_no_"))
            dataRow.setValue("corp_no_", handle.getSession().getCorpNo());
        if (!dataRow.hasValue("user_code_"))
            dataRow.setValue("user_code_", handle.getSession().getUserCode());

        return new QueueItem(this, dataRow.json());
    }

    @Override
    public final boolean consume(MessageManager manager, MessageProps message, Consumer<Boolean> action) {
        boolean result = false;
        try {
            result = this.consume(message);
            if (action != null)
                action.accept(manager.isAsync() ? true : result);
            if (manager.isAsync()) {
                // 登记执行情况到 sqlmq
                sqlmqContainer.checkNextMessage(this, (MessageData) message, result);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            message.setRemark(e.getMessage());
            if (action != null)
                action.accept(manager.isAsync() ? true : result);
            if (manager.isAsync()) {
                // 登记异常到 sqlmq
                sqlmqContainer.appendErrorMessage(this, (MessageData) message);
            }
        }
        return result;
    }

    private boolean consume(MessageProps props) {
        var message = props.getData();
        log.debug("{} 开始消费 {}", this.getId(), message);
        var data = new DataRow().setJson(message);
        try (TaskHandle handle = new TaskHandle()) {
            if (data.hasValue("token")) {
                // 临时恢复token，由队列自己实现此方法，设置Redis缓存
                this.repairToken(data.getString("token"));
                boolean loadToken = SessionFactory.loadToken(handle.getSession(), data.getString("token"));
                if (!loadToken) {
                    String error = String.format(
                            Lang.get(AbstractDataRowQueue.class, 1, "队列 token 已失效 %s，执行对象 %s，消息内容 %s"),
                            data.getString("token"), this.getClass(), message);
                    RuntimeException e = new RuntimeException(error);
                    log.error(e.getMessage(), e);
                    return true;
                }
                DataCell corpNo = data.bind("corp_no_");// 执行器的目标帐套
                DataCell userCode = data.bind("user_code_");
                if (corpNo.hasValue())
                    handle.buildSession(corpNo.getString(), userCode.getString());
            }
            var state = this.execute(handle, data, props);
            if (state.isFail())
                if (Utils.isEmpty(props.getRemark()))
                    props.setRemark(state.message());
            return state.isOk();
        }
    }

    public abstract StateMessage execute(IHandle handle, DataRow data, MessageProps props);

}
