package cn.cerc.mis.queue;

import cn.cerc.db.core.Utils;

public class CustomMessageData implements MessageValidate, MessageToken {
    private String token;

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 
     * @return 检查各项数据是否符合消息队列要求
     */
    @Override
    public boolean validate() {
        return !Utils.isEmpty(token);
    }

}
