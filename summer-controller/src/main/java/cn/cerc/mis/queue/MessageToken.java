package cn.cerc.mis.queue;

public interface MessageToken {

    String getToken();

    void setToken(String token);

}
