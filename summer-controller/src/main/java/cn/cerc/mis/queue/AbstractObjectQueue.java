package cn.cerc.mis.queue;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.Utils;
import cn.cerc.db.log.KnowallException;
import cn.cerc.db.queue.AbstractQueue;
import cn.cerc.db.queue.MessageData;
import cn.cerc.db.queue.MessageManager;
import cn.cerc.db.queue.MessageProps;
import cn.cerc.db.queue.QueueItem;
import cn.cerc.db.queue.rabbitmq.SimpleGroup;
import cn.cerc.db.tool.JsonTool;
import cn.cerc.mis.client.CorpConfigImpl;
import cn.cerc.mis.client.RemoteService;
import cn.cerc.mis.client.ServerConfigImpl;
import cn.cerc.mis.core.Application;
import cn.cerc.mis.security.SessionFactory;

public abstract class AbstractObjectQueue<T> extends AbstractQueue {
    private static final Logger log = LoggerFactory.getLogger(AbstractObjectQueue.class);

    public abstract Class<T> getClazz();

    public String appendToLocal(IHandle handle, T data) {
        SimpleGroup group = new SimpleGroup(handle);
        MessageData result = group.addItem(this.getToLocal(handle, data));
        group.start();
        return result.getMsgId();
    }

    public String appendToRemote(IHandle handle, CorpConfigImpl config, T data) {
        QueueItem item = getToRemote(handle, data, config);
        SimpleGroup group = new SimpleGroup(handle);
        group.setIndustry(config.original());
        MessageData result = group.addItem(item);
        group.start();
        return result.getMsgId();
    }

    public QueueItem getToLocal(IHandle handle, T data) {
        if (data instanceof MessageToken cmd)
            cmd.setToken(handle.getSession().getToken());
        if (data instanceof MessageValidate mv) {
            if (!mv.validate()) {
                throw new RuntimeException(String.format(
                        Lang.get(AbstractObjectQueue.class, 1, "[%s] 数据不符合消息队列要求，无法发送！ [corpNo] %s, [data] %s"),
                        this.getClazz().getSimpleName(), handle.getCorpNo(), JsonTool.toJson(data)));
            }
        }
        return new QueueItem(this, JsonTool.toJson(data));
    }

    public QueueItem getToRemote(IHandle handle, T data, CorpConfigImpl config) {
        Objects.requireNonNull(config);
        if (!config.isLocal()) {
            ServerConfigImpl serverConfig = Application.getBean(ServerConfigImpl.class);
            if (serverConfig != null) {
                try {
                    serverConfig.getIndustry(handle, config.getCorpNo()).ifPresent(this::setOriginal);
                } catch (DataException e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
        }

        // 从帐套配置中读取目标帐套行业别
//        if (Utils.isNotEmpty(config.original()))
//            this.setOriginal(config.original());

        if (!Utils.isEmpty(config.getCorpNo())) {
            Optional<ServerConfigImpl> serviceConfig = RemoteService.getServerConfig(SpringBean.context());
            if (serviceConfig.isPresent()) {
                Optional<String> remoteToken;
                try {
                    remoteToken = serviceConfig.get().getToken(handle, config.getCorpNo());
                } catch (DataException e) {
                    throw new RuntimeException(e.getMessage());
                }
                if (remoteToken.isPresent()) {
                    if (data instanceof MessageToken token)
                        token.setToken(remoteToken.get());
                }
            }
        }
        if (data instanceof MessageValidate mv) {
            if (!mv.validate()) {
                throw new RuntimeException(String.format(
                        Lang.get(AbstractObjectQueue.class, 1, "[%s] 数据不符合消息队列要求，无法发送！ [corpNo] %s, [data] %s"),
                        this.getClazz().getSimpleName(), handle.getCorpNo(), JsonTool.toJson(data)));
            }
        }
        return new QueueItem(this, JsonTool.toJson(data));
    }

    @Override
    public final boolean consume(MessageManager manager, MessageProps message, Consumer<Boolean> action) {
        boolean result = false;
        try {
            result = this.consume(message);
            if (action != null)
                action.accept(manager.isAsync() ? true : result);
            if (manager.isAsync()) {
                // 登记执行情况到 sqlmq
                sqlmqContainer.checkNextMessage(this, (MessageData) message, result);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            message.setRemark(e.getMessage());
            if (manager.isAsync()) {
                // 登记异常到 sqlmq
                sqlmqContainer.appendErrorMessage(this, (MessageData) message);
            }
        }
        return result;
    }

    protected boolean consume(MessageProps props) {
        var message = props.getData();
        T data = JsonTool.fromJson(message, getClazz());
        if (data == null)
            return true;
        try (TaskHandle handle = new TaskHandle()) {
            if (data instanceof MessageToken token) {
                if (!Utils.isEmpty(token.getToken())) {
                    this.repairToken(token.getToken());
                    boolean loadToken = SessionFactory.loadToken(handle.getSession(), token.getToken());
                    if (!loadToken) {
                        RuntimeException e = new RuntimeException();
                        log.warn("消息类 {} 的执行token失效，导致相应的业务没有被执行，请修正", this.getClass(), new KnowallException(e)
                                .addMapOf("token", token.getToken()).addMapOf("message", message));
                        return true;
                    }
                }
            }
            return this.execute(handle, data, props);
        }
    }

    public T addItem() {
        T result = null;
        try {
            result = getClazz().getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 执行消息消费
     * 
     * @return 消息消费成功否
     */
    public abstract boolean execute(IHandle handle, T entity, MessageProps props);

}
