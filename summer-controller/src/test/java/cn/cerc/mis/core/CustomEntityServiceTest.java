package cn.cerc.mis.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.Handle;

public class CustomEntityServiceTest {

    @Test
    public void test_call() {
        var svr = new StubEntityService();
        var dataIn = new DataSet();
        dataIn.head().setValue("tbNo_", "OD20010001");
        var dataOut = svr.execute(Handle.getStub(), dataIn);
        assertEquals("{\"state\":1}", dataOut.json());
    }

}
