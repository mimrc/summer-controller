package cn.cerc.mis.cache;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cn.cerc.db.queue.ServerNode;

public class ServerNodeTest {

    @Test
    public void test_1() {
        String text = "http://127.0.0.1:8080,1,fpl,240801.01,0,0,";
        var node = new ServerNode(text);
        assertEquals(text, node.toString());
    }

    @Test
    public void test_2() {
        var node = new ServerNode();
        assertEquals("", node.toString());
    }

    @Test
    public void test_3() {
        var node = new ServerNode();
        node.setHost("http://127.0.0.1:8080");
        node.setExpire(1722493375249l);
        assertEquals("http://127.0.0.1:8080,1,,,0,1722493375249,", node.toString());
    }

    @Test
    public void test_4() {
        var node = new ServerNode();
        node.setHost("http://127.0.0.1:8080");
        node.setGroup("fpl");
        node.setExpire(1722493375256l);
        assertEquals("http://127.0.0.1:8080,1,fpl,,0,1722493375256,", node.toString());
    }

}
